import { useState, useCallback, useRef } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MegaphoneIcon } from "@heroicons/react/24/outline";
import { PhotoIcon } from "@heroicons/react/24/solid";

import { positionStyles, popUpPositionStyles } from "../placement";

export default function SimpleFeedbackForm({ appKey, direction, options }) {
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState(null);
  const formRef = useRef(null);
  const inputFileRef = useRef(null);

  const handleSubmit = useCallback(async (event) => {
    setLoading(true);
    event.preventDefault();
    try {
      const data = new FormData(event.target);
      await fetch("https://zofx.eu/api/values", {
        method: "POST",
        body: data,
      });
    } catch (err) {
      throw err
    }
    setLoading(false);
  }, []);

  const handleFileAttached = useCallback((e) => {
    setFile(URL.createObjectURL(e.target.files[0]));
  }, []);

  return (
    <Popover className={`zofx-fixed ${positionStyles(direction)}`}>
      <Popover.Button
        className="zofx-rounded-full zofxcta zofx-p-2 zofx-text-black zofx-shadow-lg border-t-2 focus-visible:zofx-outline focus-visible:zofx-outline-2 focus-visible:zofx-outline-offset-2 "
        title="Send feedback"
        style={{
          "--zofx-primary": options.primaryColor,
          "--zofx-primary-dark": options.primaryDarkColor,
          "--zofx-hover-dark": options.hoverDarkColor,
          "--zofx-hover": options.hoverColor,
          "--zofx-fill": options.fillColor,
          "--zofx-fill-dark": options.fillColorDark,
        }}
      >
        <MegaphoneIcon className="zofx-w-6 zofx-h-6" />
      </Popover.Button>

      <Transition
        enter="zofx-transition zofx-duration-100 zofx-ease-out"
        enterFrom="zofx-transform zofx-scale-95 zofx-opacity-0"
        enterTo="zofx-transform zofx-scale-100 zofx-opacity-100"
        leave="zofx-transition zofx-duration-75 zofx-ease-out"
        leaveFrom="zofx-transform zofx-scale-100 zofx-opacity-100"
        leaveTo="zofx-transform zofx-scale-95 zofx-opacity-0"
      >
        <Popover.Panel
          className={`zofx-fixed zofx-box-border zofx-border-2 zofx-border-gray-400/25 dark:zofx-border-slate-800 zofx-w-[400px] zofx-bg-white dark:zofx-bg-slate-900 zofx-rounded-md zofx-shadow-lg zofx-z-10 ${popUpPositionStyles(
            direction
          )}`}
        >
          {({ close }) => (
            <form
              ref={formRef}
              className={`zofx-py-4 zofx-overflow-y-auto zofx-max-h-[82vh] `}
              onSubmit={async (ev) => {
                await handleSubmit(ev);
                close();
              }}
              style={{
                "--zofx-primary": options.primaryColor,
                "--zofx-primary-dark": options.primaryDarkColor,
                "--zofx-hover-dark": options.hoverDarkColor,
                "--zofx-hover": options.hoverColor,
                "--zofx-fill": options.fillColor,
                "--zofx-fill-dark": options.fillColorDark,
              }}
            >
              <input
                type="hidden"
                className="zofx-hidden"
                name="value[key]"
                value={appKey}
              />
              <div className="zofx-px-2 sm:zofx-px-4">
                <h2 className="zofx-text-base zofx-font-semibold zofx-leading-7 zofx-text-gray-900 dark:zofx-text-white">
                  {options.title}
                </h2>
                <p className="zofx-text-sm zofx-leading-6 zofx-text-gray-600 dark:zofx-text-white">
                  {options.subtitle}
                </p>

                <div className="zofx-mt-4 zofx-grid zofx-grid-cols-1 zofx-gap-x-6 zofx-gap-y-4 sm:zofx-grid-cols-6">
                  {options.showEmail === "true" ? (
                    <div className="sm:zofx-col-span-full">
                      <label
                        htmlFor="value[email]"
                        className="zofx-block zofx-text-sm zofx-text-start zofx-font-medium zofx-leading-6 zofx-text-gray-900 dark:zofx-text-white"
                      >
                        {options.emailLabel}
                      </label>
                      <div className="zofx-mt-2">
                        <div className="zofx-flex zofx-rounded-md zofx-shadow-sm zofx-ring-1 zofx-ring-inset zofx-ring-gray-300 sm:zofx-max-w-md">
                          <input
                            type="email"
                            name="value[email]"
                            id="value[email]"
                            autoComplete="email"
                            className="zofx-px-3 zofx-block zofx-flex-1 zofx-border-0 zofx-bg-transparent zofx-py-1.5 zofx-text-gray-900 dark:zofx-text-white placeholder:zofx-text-gray-400 dark:zofx-bg-slate-800 sm:zofx-text-sm sm:zofx-leading-6 zofx-ring-gray-300 zofx-rounded focus:zofx-ring-2 focus:zofx-ring-inset"
                            placeholder={options.emailPlaceholder}
                          />
                        </div>
                      </div>
                    </div>
                  ) : null}

                  <div className="zofx-col-span-full">
                    <label
                      htmlFor="value[content]"
                      className="zofx-block zofx-text-sm zofx-text-start zofx-font-medium zofx-leading-6 zofx-text-gray-900 dark:zofx-text-white"
                    >
                      {options.textLabel}
                    </label>
                    <div className="zofx-mt-2">
                      <textarea
                        id="value[content]"
                        name="value[content]"
                        rows={2}
                        required
                        placeholder={options.textPlaceholder}
                        className="zofx-py-2 zofx-px-3 zofx-bg-white dark:zofx-bg-slate-800 zofx-block zofx-w-full zofx-rounded-md zofx-border-0 zofx-text-gray-900 dark:zofx-text-white zofx-shadow-sm zofx-ring-1 zofx-ring-inset zofx-ring-gray-300 placeholder:zofx-text-gray-400 focus:zofx-ring-2 focus:zofx-ring-inset sm:zofx-py-1.5 sm:zofx-text-sm sm:zofx-leading-6"
                        defaultValue={""}
                      />
                    </div>
                  </div>

                  {options.showAddAttachment === "true" ? (
                    <div className="zofx-col-span-full">
                      <label
                        htmlFor="cover-photo"
                        className="zofx-block zofx-text-sm zofx-text-start zofx-font-medium zofx-leading-6 zofx-text-gray-900 dark:zofx-text-white"
                      >
                        {options.addAttachmentLabel}
                      </label>
                      <div className="zofx-relative zofx-mt-2 zofx-flex zofx-justify-center zofx-rounded-lg zofx-border zofx-border-dashed zofx-border-gray-900/25 dark:zofx-border-white/25 zofx-px-6 zofx-py-4">
                        <div className="text-center">
                          {!file ? (
                            <PhotoIcon
                              className="zofx-mx-auto zofx-h-12 zofx-w-12 zofx-text-gray-300"
                              aria-hidden="true"
                            />
                          ) : (
                            <img
                              className="zofx-h-18 zofx-w-18 zofx-m-auto zofx-rounded-md"
                              src={file}
                              alt=""
                              onClick={(ev) => {
                                ev.preventDefault();
                                setFile(null);
                                inputFileRef.value = null;
                              }}
                            />
                          )}

                          <div className="zofx-mt-4 zofx-flex zofx-text-sm zofx-leading-6 zofx-text-gray-600">
                            <label
                              htmlFor="file"
                              className="zofx-cursor-pointer zofx-rounded-md zofx-font-semibold zofx-text-brand-dark focus-within:zofx-outline-none focus-within:zofx-ring-2 focus-within:zofx-ring-brand focus-within:zofx-ring-offset-2 hover:zofx-text-brand-light"
                            >
                              <span className="zofxlabel">{options.attachmentUploadButton}</span>
                              <input
                                id="file"
                                ref={inputFileRef}
                                name="file"
                                type="file"
                                className="zofx-absolute zofx-w-full zofx-h-full zofx-top-0 zofx-bottom-0 zofx-left-0 zofx-right-0 zofx-opacity-0 zofx-cursor-pointer"
                                accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps,image/heic,image/heif,video/quicktime,video/h264,video/mp4,video/ogg,video/webm"
                                onChange={handleFileAttached}
                              />
                            </label>
                            <p className="zofx-pl-1 dark:zofx-text-white">
                              {options.attachmentUploadCaption}
                            </p>
                          </div>

                          <p className="zofx-text-xs zofx-leading-5 zofx-text-gray-600 dark:zofx-text-white">
                            {options.attachmentLimits}
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="zofx-mt-6 zofx-flex zofx-items-center zofx-px-2 sm:zofx-px-4">
                <button
                  type="submit"
                  className={`zofxctalabel zofx-inline-flex zofx-items-center zofx-justify-center zofx-rounded-md zofx-w-full  zofx-px-3 zofx-py-2 zofx-text-sm zofx-font-semibold zofx-shadow-sm  ${
                    loading
                      ? "zofxdisabled zofx-cursor-not-allowed"
                      : "zofxcta focus-visible:zofx-outline focus-visible:zofx-outline-2 focus-visible:zofx-outline-offset-2 "
                  }`}
                >
                  {loading ? (
                    <svg
                      className="zofx-animate-spin zofx-mr-3 zofx-h-5 zofx-w-5 zofx--ml-4"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                    >
                      <circle
                        className="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        strokeWidth="4"
                      ></circle>
                      <path
                        className="zofx-opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                      ></path>
                    </svg>
                  ) : null}
                  {options.submitButtonLabel}
                </button>
              </div>
            </form>
          )}
        </Popover.Panel>
      </Transition>
    </Popover>
  );
}
