import SimpleFeedbackForm from "./components/SimpleFeedbackForm";

function ZofxFeedback({ appKey, direction, options }) {
  return (
    <SimpleFeedbackForm
      appKey={appKey}
      direction={direction}
      options={options}
    />
  );
}

export default ZofxFeedback;
